# Fab's Sentry On-Premise
This image is based on sentry but also includes the Gitlab Oauth plugin: sentry-auth-gitlab

## Requirements

 * Docker 1.10.0+

## Resources

 * [Documentation](https://docs.sentry.io/server/installation/docker/)
 * [Bug Tracker](https://github.com/getsentry/onpremise)
 * [Forums](https://forum.sentry.io/c/on-premise)
 * [IRC](irc://chat.freenode.net/sentry) (chat.freenode.net, #sentry)
