FROM sentry:9.0-onbuild

COPY sentry.conf.py /etc/sentry
COPY requirements.txt /tmp/requirements.txt
